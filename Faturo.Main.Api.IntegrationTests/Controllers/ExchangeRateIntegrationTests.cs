﻿using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Faturo.Main.Api.IntegrationTests.Factories;
using Faturo.Main.Api.IntegrationTests.Fixtures;
using FluentAssertions;
using System.Net;
using System.Net.Http.Json;
using Xunit.Extensions.Ordering;

namespace Fatturo.Main.Api.IntegrationTests.Controller;

[Collection("Database")]
public class ExchangeRateIntegrationTests: IClassFixture<FatturoWebApplicationFactory>, IClassFixture<ExchangeRateDataGeneratingFixture>
{
    private readonly FatturoWebApplicationFactory _factory;
    private readonly HttpClient _client;
    private readonly ExchangeRateDataGeneratingFixture _fixture;

    public ExchangeRateIntegrationTests(FatturoWebApplicationFactory factory, ExchangeRateDataGeneratingFixture fixture)
    {
        _factory = factory;
        _client = _factory.CreateClient();
        _fixture = fixture;
    }

    [Fact(DisplayName ="Create Exchange Rate : Should Return Created")]
    [Trait("Category","IntegrationTests")]
    [Order(1)]
    public async Task CreateExchangeRate_WhenCommandIsValid_ShouldReturnCreatedAtRoute()
    {
        //Arrange
        var request = _fixture.GenerateValidExchangeRateCommand();
        
        //Act
        var response = await _client.PostAsJsonAsync("api/exchangerates", request);
        var exchangeRate = await response.Content.ReadFromJsonAsync<CreateExchangeRate>();

        //Assert
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        exchangeRate?.Should().NotBeNull();
        _fixture.CreatedExchangeRateId = exchangeRate!.Id;
    }

    [Fact(DisplayName = "Get Exchange Rate By Id : Should Return ExchangeRate")]
    [Trait("Category", "IntegrationTests")]
    [Order(1)]
    public async Task GetExchangeRateById_WhenExchangeRateInDb_ShouldReturnExchangeRate()
    {
        //Arrange
        var exchangeRateId = _fixture.CreatedExchangeRateId;

        //Act
        var response = await _client.GetAsync($"api/exchangerates/{exchangeRateId}");
        var exchangeRate = await response.Content.ReadFromJsonAsync<ExchangeRateDto>();

        //Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        exchangeRate?.Id.Should().Be(exchangeRateId);
    }
}


