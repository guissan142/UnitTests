using Faturo.Main.Api.IntegrationTests.Fixtures;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace Faturo.Main.Api.IntegrationTests.Factories;

[Collection("Database")]
public class FatturoWebApplicationFactory: WebApplicationFactory<Program>
{
    private readonly DbFixture _dbFixture;

    public FatturoWebApplicationFactory(DbFixture dbFixture)
    {
        _dbFixture = dbFixture;
    }

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureAppConfiguration((context, configuration) =>
        {
            configuration.AddInMemoryCollection(new[]
            {
                new KeyValuePair<string,string?>("ConnectionStrings:ApplicationConnection",_dbFixture.ConnectionString),
            });
        });
    }
}
