﻿using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faturo.Main.Api.IntegrationTests.Fixtures
{
    public class ExchangeRateDataGeneratingFixture : IDisposable
    {
        public Guid CreatedExchangeRateId;

        public CreateExchangeRateCommand GenerateValidExchangeRateCommand()
        {
            return new CreateExchangeRateCommand
            {
                QuotationDate = DateOnly.FromDateTime(DateTime.Parse("2023-10-21")),
                Factor = 2.5m,
                Type = "Ptax",
                CurrencyOriginId = Guid.Parse("003aa9e3-0265-42f6-8117-a7b17d2f62b9"),
                CurrencyDestinationId = Guid.Parse("16fceb58-96af-4bbf-9748-0ea76f7fd0dd")
            };
        }
        public void Dispose()
        {
        }
    }
}
