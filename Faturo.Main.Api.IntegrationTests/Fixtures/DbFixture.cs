using Fatturo.Main.Persistence.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace Faturo.Main.Api.IntegrationTests.Fixtures;

[CollectionDefinition("Database")]
public class DatabaseCollection : ICollectionFixture<DbFixture> { }

public class DbFixture : IDisposable
{
    private readonly ApplicationDbContext _context;
    public readonly string DatabaseName = $"Context - {Guid.NewGuid()}";
    public readonly string ConnectionString;
    private bool _disposed;

    public DbFixture()
    {
        ConnectionString = $"Host = localhost;Database = {DatabaseName};Username=postgres;Password=123456;Include Error Detail = true";
        var builder = new DbContextOptionsBuilder<ApplicationDbContext>();

        builder.UseNpgsql(ConnectionString) ;
        _context = new ApplicationDbContext(builder.Options);
        _context.Database.Migrate();
    }
    public void Dispose()
    {
       Dispose(true);
       GC.SuppressFinalize(this);
    }
    protected virtual void Dispose(bool disposing)
    {
        if(!_disposed)
        {
            if(disposing)
            {
                _context.Database.EnsureDeleted();
            }
            _disposed = true;
        }
    }
}