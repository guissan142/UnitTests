﻿
using Fatturo.Main.Domain.Common;
using Fatturo.Main.Domain.Entities.Base;
using Fatturo.Main.Domain.Utils;

namespace Fatturo.Main.Domain.Entities
{
    public sealed class ExchangeRate : BaseEntity
    {
        public DateOnly QuotationDate { get; private set; }
        public decimal Factor { get; private set; }
        public ExchangeRateType Type { get; private set; }
        public Guid CurrencyOriginId { get; private set; }
        public Currency CurrencyOrigin { get; private set; } = null!;
        public Guid CurrencyDestinationId { get; private set; }
        public Currency CurrencyDestination { get; private set; } = null!;

        private ExchangeRate() { }

        public ExchangeRate(
            DateOnly quotationDate, 
            decimal factor, 
            string type, 
            Guid currencyOriginId, 
            Guid currencyDestinationId)
        {
            QuotationDate = quotationDate;
            Factor = RoundFactorValue(factor);
            Type = ConvertToEnum(type);
            CurrencyOriginId = currencyOriginId;
            CurrencyDestinationId = currencyDestinationId;

            Validate();
        }

        public void Update(
            DateOnly quotationDate,
            decimal factor,
            string type,
            Guid currencyOriginId,
            Guid currencyDestinationId)
        {
            QuotationDate = quotationDate;
            Factor = RoundFactorValue(factor);
            Type = ConvertToEnum(type);
            CurrencyOriginId = currencyOriginId;
            CurrencyDestinationId = currencyDestinationId;

            Validate();
        }

        private ExchangeRateType ConvertToEnum(string type)
        {
            if(!EnumUtils.IsInEnum<ExchangeRateType>(type))
            {
                throw new DomainException("Type must be Valid.");
            }

            return (ExchangeRateType)Enum.Parse(typeof(ExchangeRateType), type);
        }

        private decimal RoundFactorValue(decimal factor)
        {
            return Math.Round(factor, 5);
        }

        public void Validate()
        {
            AssertionConcern.AssertArgumentLessThan(
                QuotationDate,
                DateOnly.FromDateTime(DateTime.UtcNow),
                "The QuotationDate cannot be in the future");

            AssertionConcern.AssertArgumentGreaterThan(
                Factor,
                0,
                "The Factor cannot be zero");

            AssertionConcern.AssertArgumentNotEmpty(
                CurrencyOriginId,
                "The CurrencyOriginId cannot be empty");

            AssertionConcern.AssertArgumentNotEmpty(
                CurrencyDestinationId,
                "The CurrencyDestinationId cannot be empty");
        }
    }

    public enum ExchangeRateType
    {
        Ptax = 1,

        Open = 4,

        Close = 9,
    }
}
