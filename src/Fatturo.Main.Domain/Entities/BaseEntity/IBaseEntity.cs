﻿
using Fatturo.Main.Domain.Common;

namespace Fatturo.Main.Domain.Entities.Base
{
    public interface IBaseEntity
    {
        Guid Id { get; }
    }
}
