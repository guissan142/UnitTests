﻿using Fatturo.Main.Domain.Entities;

namespace Fatturo.Main.Domain.Utils
{
    public class EnumUtils
    {
        public static bool IsInEnum<T>(string value)
        {
            return Enum.TryParse(typeof(T), value, out object? result);
        }

        public static ExchangeRateType ConvertToEnum(string type)
        {
            return (ExchangeRateType)Enum.Parse(typeof(ExchangeRateType), type);
        }
    }
}
