﻿using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter;
using Fatturo.Main.Domain.Entities;
using Fatturo.Main.Persistence.DbContexts;
using Microsoft.EntityFrameworkCore;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Domain.Utils;

namespace Fatturo.Main.Persistence.Repositories
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly ApplicationDbContext _context;

        public ExchangeRateRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void AddExchangeRate(ExchangeRate exchangeRate)
        {
            _context.ExchangeRates.Add(exchangeRate);
        }

        public void DeleteExchangeRate(ExchangeRate exchangeRate)
        {
            _context.ExchangeRates.Remove(exchangeRate);
        }

        public async Task<bool> ExchangeRateExistsAsync(CreateExchangeRateCommand exchangeRate)
        {
            return await _context.ExchangeRates.AnyAsync(p =>
                p.QuotationDate == exchangeRate.QuotationDate &&
                p.Type == EnumUtils.ConvertToEnum(exchangeRate.Type) &&
                p.CurrencyOriginId == exchangeRate.CurrencyOriginId &&
                p.CurrencyDestinationId == exchangeRate.CurrencyDestinationId
            );
        }

        public async Task<bool> SameExchangeRateDataExistsAsync(UpdateExchangeRateCommand exchangeRate)
        {
            return await _context.ExchangeRates.AnyAsync(p => 
                p.Id != exchangeRate.Id && 
                p.QuotationDate == exchangeRate.QuotationDate &&
                p.Type == EnumUtils.ConvertToEnum(exchangeRate.Type) &&
                p.CurrencyOriginId == exchangeRate.CurrencyOriginId &&
                p.CurrencyDestinationId == exchangeRate.CurrencyDestinationId
            );
        }

        public async Task<ExchangeRate?> GetExchangeRateByIdAsync(Guid id)
        {
            return await _context.ExchangeRates
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IEnumerable<ExchangeRate>> GetExchangeRatesByMacroFilterAsync(
            GetExchangeRatesByMacroFilterQuery getExchangeRatesByMacroFilterQuery)
        {
            return await _context.ExchangeRates
                .Where(p => p
                    .QuotationDate >= getExchangeRatesByMacroFilterQuery.quotationDateStart
                    && p.QuotationDate <= getExchangeRatesByMacroFilterQuery.quotationDateEnd
                    && getExchangeRatesByMacroFilterQuery.type.Contains((int)p.Type)
                    && p.CurrencyOriginId == getExchangeRatesByMacroFilterQuery.currencyOriginId
                    && p.CurrencyDestinationId == getExchangeRatesByMacroFilterQuery.currencyDestinationId
                )
                .ToListAsync();
        }

        public async Task<bool> CurrencyExistsAsync(Guid currencyId)
        {
            return await _context.Currency.AnyAsync(p => p.Id == currencyId);
        }
    }
}
