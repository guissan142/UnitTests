﻿using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Domain.Entities;
using Fatturo.Main.Persistence.DbContexts;
using Fatturo.Main.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Fatturo.Main.Persistence.Extensions
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(
            this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRepositoryServices();
            services.AddDbContexts(configuration);

            return services;
        }

        private static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddScoped<IExchangeRateRepository, ExchangeRateRepository>();
        }

        private static void AddDbContexts(this IServiceCollection services,IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("ApplicationConnection"))
                    .EnableSensitiveDataLogging()
            );
        }
    }
}
