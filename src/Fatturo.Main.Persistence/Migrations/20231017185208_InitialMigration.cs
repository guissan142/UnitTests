﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Fatturo.Main.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericValue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericValue", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExchangeRates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    QuotationDate = table.Column<DateOnly>(type: "date", nullable: false),
                    Factor = table.Column<decimal>(type: "numeric(10,5)", precision: 10, scale: 5, nullable: false),
                    TypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    CurrencyOriginId = table.Column<Guid>(type: "uuid", nullable: false),
                    CurrencyDestinationId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExchangeRates_Currency_CurrencyDestinationId",
                        column: x => x.CurrencyDestinationId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExchangeRates_Currency_CurrencyOriginId",
                        column: x => x.CurrencyOriginId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExchangeRates_GenericValue_TypeId",
                        column: x => x.TypeId,
                        principalTable: "GenericValue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_CurrencyDestinationId",
                table: "ExchangeRates",
                column: "CurrencyDestinationId");

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_CurrencyOriginId",
                table: "ExchangeRates",
                column: "CurrencyOriginId");

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_QuotationDate",
                table: "ExchangeRates",
                column: "QuotationDate");

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_TypeId",
                table: "ExchangeRates",
                column: "TypeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExchangeRates");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "GenericValue");
        }
    }
}
