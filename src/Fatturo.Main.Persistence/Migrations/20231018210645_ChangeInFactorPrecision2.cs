﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Fatturo.Main.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class ChangeInFactorPrecision2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("2d8fd865-46ad-4859-94fb-acc8f580a358"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("51b83d40-19a1-4d17-b689-31dcb7b0ce7e"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("9060cbf5-0f4d-4783-9812-dd9b72145f18"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("be065b46-e569-4714-970e-f6381934f24c"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("e0f16cec-27c6-4630-8de5-1c5fb66f5d32"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"));

            migrationBuilder.AlterColumn<decimal>(
                name: "Factor",
                table: "ExchangeRates",
                type: "numeric(1000,5)",
                precision: 1000,
                scale: 5,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(5,0)",
                oldPrecision: 5);

            migrationBuilder.InsertData(
                table: "Currency",
                column: "Id",
                values: new object[]
                {
                    new Guid("003aa9e3-0265-42f6-8117-a7b17d2f62b9"),
                    new Guid("16fceb58-96af-4bbf-9748-0ea76f7fd0dd"),
                    new Guid("8ee3a989-eb16-472e-8d1f-aff4ab2bdc82"),
                    new Guid("91f30ba8-867d-43e7-a2dc-754a36988547"),
                    new Guid("d5769e4e-043f-457b-a5ce-18cc7c488074"),
                    new Guid("e2a0875b-64c0-49a2-8058-6feaf19e2507")
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrencyDestinationId", "CurrencyOriginId", "Factor", "QuotationDate", "Type" },
                values: new object[,]
                {
                    { new Guid("1853582f-c62e-4da4-9368-f4076018d80c"), new Guid("e2a0875b-64c0-49a2-8058-6feaf19e2507"), new Guid("8ee3a989-eb16-472e-8d1f-aff4ab2bdc82"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("81f4899c-e4aa-4cb0-8c57-8d249f73a4fa"), new Guid("91f30ba8-867d-43e7-a2dc-754a36988547"), new Guid("003aa9e3-0265-42f6-8117-a7b17d2f62b9"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("b588f640-ce11-4d12-b524-72a3e4510eda"), new Guid("d5769e4e-043f-457b-a5ce-18cc7c488074"), new Guid("16fceb58-96af-4bbf-9748-0ea76f7fd0dd"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("b6169bc8-ce96-4552-a831-8a5b2b0194ab"), new Guid("8ee3a989-eb16-472e-8d1f-aff4ab2bdc82"), new Guid("d5769e4e-043f-457b-a5ce-18cc7c488074"), 10m, new DateOnly(2023, 10, 16), "Ptax" },
                    { new Guid("b916fc36-95d3-41cc-96d7-63d1a090cce5"), new Guid("003aa9e3-0265-42f6-8117-a7b17d2f62b9"), new Guid("e2a0875b-64c0-49a2-8058-6feaf19e2507"), 10m, new DateOnly(2023, 10, 16), "Close" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("1853582f-c62e-4da4-9368-f4076018d80c"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("81f4899c-e4aa-4cb0-8c57-8d249f73a4fa"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("b588f640-ce11-4d12-b524-72a3e4510eda"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("b6169bc8-ce96-4552-a831-8a5b2b0194ab"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("b916fc36-95d3-41cc-96d7-63d1a090cce5"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("003aa9e3-0265-42f6-8117-a7b17d2f62b9"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("16fceb58-96af-4bbf-9748-0ea76f7fd0dd"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("8ee3a989-eb16-472e-8d1f-aff4ab2bdc82"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("91f30ba8-867d-43e7-a2dc-754a36988547"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("d5769e4e-043f-457b-a5ce-18cc7c488074"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("e2a0875b-64c0-49a2-8058-6feaf19e2507"));

            migrationBuilder.AlterColumn<decimal>(
                name: "Factor",
                table: "ExchangeRates",
                type: "numeric(5,0)",
                precision: 5,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(1000,5)",
                oldPrecision: 1000,
                oldScale: 5);

            migrationBuilder.InsertData(
                table: "Currency",
                column: "Id",
                values: new object[]
                {
                    new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"),
                    new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"),
                    new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"),
                    new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"),
                    new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"),
                    new Guid("e87492a2-d6ba-4413-87d7-a08d37841437")
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrencyDestinationId", "CurrencyOriginId", "Factor", "QuotationDate", "Type" },
                values: new object[,]
                {
                    { new Guid("2d8fd865-46ad-4859-94fb-acc8f580a358"), new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"), new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"), 10m, new DateOnly(2023, 10, 16), "Ptax" },
                    { new Guid("51b83d40-19a1-4d17-b689-31dcb7b0ce7e"), new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"), new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("9060cbf5-0f4d-4783-9812-dd9b72145f18"), new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"), new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("be065b46-e569-4714-970e-f6381934f24c"), new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"), new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("e0f16cec-27c6-4630-8de5-1c5fb66f5d32"), new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"), new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"), 10m, new DateOnly(2023, 10, 16), "Close" }
                });
        }
    }
}
