﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Fatturo.Main.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class ChangeInFactorPrecision : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("19830df7-9ac1-4ed4-96ba-4164cdafa74b"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("370d1353-5fa3-4f3c-8ddd-cb7f7e01c24b"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("413f75e2-5e39-4089-b503-1ed4f39a0b27"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("8858ac42-d54a-4f29-8cff-d0a1f888a804"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("ee2f7d68-9edd-43ec-95ef-b8596ef7fa3c"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("47706254-181e-4ef3-933c-9d6f5bc156c3"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("4cf8aca3-4075-4a12-9b5d-a03c63d2b407"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("76b7ba00-b12b-4657-962b-0bcf8df56d84"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("b9867d8c-f1ea-40f7-94c4-9796a5c2de98"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("c049b3f9-6811-41cb-aa83-a82abe26f938"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("f38838ad-a099-4712-8208-5e67f6217cef"));

            migrationBuilder.AlterColumn<decimal>(
                name: "Factor",
                table: "ExchangeRates",
                type: "numeric(5)",
                precision: 5,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(10,5)",
                oldPrecision: 10,
                oldScale: 5);

            migrationBuilder.InsertData(
                table: "Currency",
                column: "Id",
                values: new object[]
                {
                    new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"),
                    new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"),
                    new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"),
                    new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"),
                    new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"),
                    new Guid("e87492a2-d6ba-4413-87d7-a08d37841437")
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrencyDestinationId", "CurrencyOriginId", "Factor", "QuotationDate", "Type" },
                values: new object[,]
                {
                    { new Guid("2d8fd865-46ad-4859-94fb-acc8f580a358"), new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"), new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"), 10m, new DateOnly(2023, 10, 16), "Ptax" },
                    { new Guid("51b83d40-19a1-4d17-b689-31dcb7b0ce7e"), new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"), new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("9060cbf5-0f4d-4783-9812-dd9b72145f18"), new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"), new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("be065b46-e569-4714-970e-f6381934f24c"), new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"), new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("e0f16cec-27c6-4630-8de5-1c5fb66f5d32"), new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"), new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"), 10m, new DateOnly(2023, 10, 16), "Close" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("2d8fd865-46ad-4859-94fb-acc8f580a358"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("51b83d40-19a1-4d17-b689-31dcb7b0ce7e"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("9060cbf5-0f4d-4783-9812-dd9b72145f18"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("be065b46-e569-4714-970e-f6381934f24c"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("e0f16cec-27c6-4630-8de5-1c5fb66f5d32"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("0d499760-2a7c-42e0-8fb8-7856637d9630"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("3f0f4548-f689-4714-b549-f220fb8e45d9"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("c8e1eeed-0b67-4f63-9d36-8e5d513d4641"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("ced5d317-4941-4326-9f09-ec9451658dbf"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("d8b4b151-61d3-4041-8aef-d2e1ed620519"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("e87492a2-d6ba-4413-87d7-a08d37841437"));

            migrationBuilder.AlterColumn<decimal>(
                name: "Factor",
                table: "ExchangeRates",
                type: "numeric(10,5)",
                precision: 10,
                scale: 5,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(5)",
                oldPrecision: 5);

            migrationBuilder.InsertData(
                table: "Currency",
                column: "Id",
                values: new object[]
                {
                    new Guid("47706254-181e-4ef3-933c-9d6f5bc156c3"),
                    new Guid("4cf8aca3-4075-4a12-9b5d-a03c63d2b407"),
                    new Guid("76b7ba00-b12b-4657-962b-0bcf8df56d84"),
                    new Guid("b9867d8c-f1ea-40f7-94c4-9796a5c2de98"),
                    new Guid("c049b3f9-6811-41cb-aa83-a82abe26f938"),
                    new Guid("f38838ad-a099-4712-8208-5e67f6217cef")
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrencyDestinationId", "CurrencyOriginId", "Factor", "QuotationDate", "Type" },
                values: new object[,]
                {
                    { new Guid("19830df7-9ac1-4ed4-96ba-4164cdafa74b"), new Guid("47706254-181e-4ef3-933c-9d6f5bc156c3"), new Guid("4cf8aca3-4075-4a12-9b5d-a03c63d2b407"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("370d1353-5fa3-4f3c-8ddd-cb7f7e01c24b"), new Guid("c049b3f9-6811-41cb-aa83-a82abe26f938"), new Guid("b9867d8c-f1ea-40f7-94c4-9796a5c2de98"), 10m, new DateOnly(2023, 10, 16), "Ptax" },
                    { new Guid("413f75e2-5e39-4089-b503-1ed4f39a0b27"), new Guid("4cf8aca3-4075-4a12-9b5d-a03c63d2b407"), new Guid("76b7ba00-b12b-4657-962b-0bcf8df56d84"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("8858ac42-d54a-4f29-8cff-d0a1f888a804"), new Guid("b9867d8c-f1ea-40f7-94c4-9796a5c2de98"), new Guid("f38838ad-a099-4712-8208-5e67f6217cef"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("ee2f7d68-9edd-43ec-95ef-b8596ef7fa3c"), new Guid("76b7ba00-b12b-4657-962b-0bcf8df56d84"), new Guid("c049b3f9-6811-41cb-aa83-a82abe26f938"), 10m, new DateOnly(2023, 10, 16), "Close" }
                });
        }
    }
}
