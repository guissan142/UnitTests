﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Fatturo.Main.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class SeedDataAdded : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Currency",
                column: "Id",
                values: new object[]
                {
                    new Guid("00692e75-e3aa-4f34-97ef-3d7d875c6b75"),
                    new Guid("0ceb7ef6-418d-4736-afc4-ce593d9539af"),
                    new Guid("51b380a4-8ad0-4222-bdf2-cc56463d2aef"),
                    new Guid("c20e0cc0-b4d3-4563-94bc-293d5ce92610"),
                    new Guid("c56b1a5b-2107-4c95-8007-6b5fdca257cc"),
                    new Guid("f8212a0f-ecd8-4e6c-862a-d8ab76c3747a")
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "CurrencyDestinationId", "CurrencyOriginId", "Factor", "QuotationDate", "Type" },
                values: new object[,]
                {
                    { new Guid("0764e735-20fd-4376-8930-2b24d1e0d99d"), new Guid("00692e75-e3aa-4f34-97ef-3d7d875c6b75"), new Guid("c56b1a5b-2107-4c95-8007-6b5fdca257cc"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("30e0c464-7263-4ba5-9a8c-69b2d67ab9cf"), new Guid("51b380a4-8ad0-4222-bdf2-cc56463d2aef"), new Guid("f8212a0f-ecd8-4e6c-862a-d8ab76c3747a"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("388466ea-496e-4fce-992d-c454d6a96a51"), new Guid("f8212a0f-ecd8-4e6c-862a-d8ab76c3747a"), new Guid("00692e75-e3aa-4f34-97ef-3d7d875c6b75"), 10m, new DateOnly(2023, 10, 16), "Close" },
                    { new Guid("d94184f6-c651-4f49-8ea4-ba030ad954ae"), new Guid("c56b1a5b-2107-4c95-8007-6b5fdca257cc"), new Guid("0ceb7ef6-418d-4736-afc4-ce593d9539af"), 10m, new DateOnly(2023, 10, 16), "Ptax" },
                    { new Guid("e996c512-f3ba-4df4-b063-9da52be6ab2d"), new Guid("0ceb7ef6-418d-4736-afc4-ce593d9539af"), new Guid("c20e0cc0-b4d3-4563-94bc-293d5ce92610"), 10m, new DateOnly(2023, 10, 16), "Close" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("0764e735-20fd-4376-8930-2b24d1e0d99d"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("30e0c464-7263-4ba5-9a8c-69b2d67ab9cf"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("388466ea-496e-4fce-992d-c454d6a96a51"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("d94184f6-c651-4f49-8ea4-ba030ad954ae"));

            migrationBuilder.DeleteData(
                table: "ExchangeRates",
                keyColumn: "Id",
                keyValue: new Guid("e996c512-f3ba-4df4-b063-9da52be6ab2d"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("00692e75-e3aa-4f34-97ef-3d7d875c6b75"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("0ceb7ef6-418d-4736-afc4-ce593d9539af"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("51b380a4-8ad0-4222-bdf2-cc56463d2aef"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("c20e0cc0-b4d3-4563-94bc-293d5ce92610"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("c56b1a5b-2107-4c95-8007-6b5fdca257cc"));

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: new Guid("f8212a0f-ecd8-4e6c-862a-d8ab76c3747a"));
        }
    }
}
