﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Fatturo.Main.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExchangeRates_GenericValue_TypeId",
                table: "ExchangeRates");

            migrationBuilder.DropTable(
                name: "GenericValue");

            migrationBuilder.DropIndex(
                name: "IX_ExchangeRates_TypeId",
                table: "ExchangeRates");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "ExchangeRates");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "ExchangeRates",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "ExchangeRates");

            migrationBuilder.AddColumn<Guid>(
                name: "TypeId",
                table: "ExchangeRates",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "GenericValue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericValue", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_TypeId",
                table: "ExchangeRates",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExchangeRates_GenericValue_TypeId",
                table: "ExchangeRates",
                column: "TypeId",
                principalTable: "GenericValue",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
