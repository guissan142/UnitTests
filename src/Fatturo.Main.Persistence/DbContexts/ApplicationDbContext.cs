﻿using Fatturo.Main.Domain.Entities;
using Fatturo.Main.Persistence.DbContexts.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Fatturo.Main.Persistence.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) { }

        public DbSet<ExchangeRate> ExchangeRates {get; set;}
        public DbSet<Currency> Currency {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new ExchangeRateConfiguration());

            var currency = builder.Entity<Currency>();
            var exchangeRate = builder.Entity<ExchangeRate>();

        var currencyList = new List<Currency>()
        {
            new Currency(),
            new Currency(),
            new Currency(),
            new Currency(),
            new Currency(),
            new Currency(),
        };
        
        var exchangeRateList = new List<ExchangeRate>()
        {
            new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                10M, "Close",
                currencyList[0].Id,
                currencyList[1].Id),
            new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                10M, "Ptax",
                currencyList[1].Id,
                currencyList[2].Id),
            new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                10M, "Close",
                currencyList[2].Id,
                currencyList[3].Id),
            new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                10M, "Close",
                currencyList[3].Id,
                currencyList[4].Id){
            },
            new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                10M, "Close",
                currencyList[4].Id,
                currencyList[5].Id),
        };
        
        currency.HasData(currencyList);
        exchangeRate.HasData(exchangeRateList);
        
        }
    }
}
