﻿using System.Linq.Expressions;
using Fatturo.Main.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Fatturo.Main.Persistence.DbContexts.Configuration
{
    public class ExchangeRateConfiguration : IEntityTypeConfiguration<ExchangeRate>
    {
        public void Configure(EntityTypeBuilder<ExchangeRate> builder)
        {
            // Indexes
            builder.HasIndex(e => e.QuotationDate);

            builder.Property(e => e.Factor)
                .IsRequired()
                .HasPrecision(1000,5);

            builder.Property(e => e.Type)
                .IsRequired()
                .HasConversion(typeof(TypeToStringConverter));
        }
    }

    public class TypeToStringConverter : ValueConverter<ExchangeRateType, string>
    {
        public TypeToStringConverter() : base(TypeString, TypeEnum)
        {
        }

        private static Expression<Func<ExchangeRateType, string>> TypeString = v => new String(v.ToString());

        private static Expression<Func<string, ExchangeRateType>> TypeEnum = v => Enum
            .GetValues<ExchangeRateType>()
            .FirstOrDefault(p => p.ToString() == v);
    }

}
    

