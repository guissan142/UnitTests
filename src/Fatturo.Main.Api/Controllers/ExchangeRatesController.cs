﻿using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Fatturo.Main.Api.Controllers
{
    [Route("api/exchangerates")]
    public class ExchangeRatesController : MainController
    {
        private readonly IMediator _mediator;
        public ExchangeRatesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{exchangeRateId}", Name = "GetExchangeRateById")]

        public async Task<ActionResult<ExchangeRateDto>> GetExchangeRateById(Guid exchangeRateId)
        {
            var getExchangeRatesByIdQueryResponse = await _mediator.Send(new GetExchangeRateByIdQuery(exchangeRateId));
            if (!getExchangeRatesByIdQueryResponse.IsSuccess)
            {
                return HandleRequestError(getExchangeRatesByIdQueryResponse);
            }
            return Ok(getExchangeRatesByIdQueryResponse.ExchangeRate);
        }

        [HttpPost]
        public async Task<ActionResult<CreateExchangeRate>> CreateExchangeRate(
            CreateExchangeRateCommand createExchangeRateCommand)
        {
            var createExchangeRatesCommandResponse = await _mediator.Send(createExchangeRateCommand);
            if (!createExchangeRatesCommandResponse.IsSuccess)
            {
                return HandleRequestError(createExchangeRatesCommandResponse);
            }
            return CreatedAtRoute(
                "GetExchangeRateById",
                new { exchangeRateId = createExchangeRatesCommandResponse.ExchangeRate.Id },
                createExchangeRatesCommandResponse.ExchangeRate
            );
        }

        [HttpDelete("{exchangeRateId}")]
        public async Task<ActionResult> DeleteExchangeRate(Guid exchangeRateId)
        {
            var deleteExchangeRatesCommandResponse = await _mediator.Send(new DeleteExchangeRatesCommand(exchangeRateId));
            if (!deleteExchangeRatesCommandResponse.IsSuccess)
            {
                return HandleRequestError(deleteExchangeRatesCommandResponse);
            }

            return NoContent();
        }

        [HttpPut("{exchangeRateId}")]
        public async Task<ActionResult> UpdateExchangeRate(Guid exchangeRateId, UpdateExchangeRateCommand updateExchangeRatesCommand)
        {
            if (exchangeRateId != updateExchangeRatesCommand.Id)
            {
                return BadRequest();
            }
            var updateExchangeRatesCommandResponse = await _mediator.Send(updateExchangeRatesCommand);
            if (!updateExchangeRatesCommandResponse.IsSuccess)
            {
                return HandleRequestError(updateExchangeRatesCommandResponse);
            }

            return NoContent();
        }
    }
}
