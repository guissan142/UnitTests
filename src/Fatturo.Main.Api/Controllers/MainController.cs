﻿using System.Diagnostics;
using Fatturo.Main.Application.Features.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;

namespace Fatturo.Main.Api.Controllers;

[ApiController]
public abstract class MainController : ControllerBase
{
    public ActionResult HandleRequestError(BaseResponse response)
    {
        ConfigureModelState(response.Errors);

        switch (response.ErrorType)
        {
            case Error.ValidationProblem:
                return UnprocessableEntity(ModelState);
            case Error.NotFoundProblem:
                return NotFound(ModelState);
            case Error.BadRequestProblem:
                return BadRequest(ModelState);
            default:
                return StatusCode(StatusCodes.Status418ImATeapot);
        }
    }
    public override ActionResult ValidationProblem(
    [ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
    {
        var options = HttpContext.RequestServices
            .GetRequiredService<IOptions<ApiBehaviorOptions>>();

        return (ActionResult)options.Value
            .InvalidModelStateResponseFactory(ControllerContext);
    }

    public override NotFoundObjectResult NotFound([ActionResultObjectValue] object? value)
    {
        var problemDetailsFactory = HttpContext.RequestServices
            .GetRequiredService<ProblemDetailsFactory>();

        var validationProblemDetails = problemDetailsFactory
            .CreateValidationProblemDetails(
                HttpContext,
                ModelState!);

        validationProblemDetails.Detail =
            "See the errors field for details.";
        validationProblemDetails.Instance =
            HttpContext.Request.Path;

        validationProblemDetails.Type =
            "https://allog.com.br/notfoundproblem";
        validationProblemDetails.Status =
            StatusCodes.Status404NotFound;
        validationProblemDetails.Title =
            "One or more records were not found";
        return new NotFoundObjectResult(
            validationProblemDetails)
        {
            ContentTypes = { "application/problem+json" }
        };

    }

    public override BadRequestObjectResult BadRequest(ModelStateDictionary modelStateDictionary)
    {
        var problemDetailsFactory = HttpContext.RequestServices
            .GetRequiredService<ProblemDetailsFactory>();

        var validationProblemDetails = problemDetailsFactory
            .CreateValidationProblemDetails(
                HttpContext,
                ModelState!);

        validationProblemDetails.Detail =
            "See the errors field for details.";
        validationProblemDetails.Instance =
            HttpContext.Request.Path;

        validationProblemDetails.Type =
            "https://allog.com.br/badrequestproblem";
        validationProblemDetails.Status =
            StatusCodes.Status400BadRequest;
        validationProblemDetails.Title =
            "One or more issues were found within the request body";
        return new BadRequestObjectResult(
            validationProblemDetails)
        {
            ContentTypes = { "application/problem+json" }
        };

    }

    public override UnprocessableEntityObjectResult UnprocessableEntity([
        ActionResultObjectValue] ModelStateDictionary modelState)
    {
        var problemDetailsFactory = HttpContext.RequestServices
            .GetRequiredService<ProblemDetailsFactory>();

        var validationProblemDetails = problemDetailsFactory
            .CreateValidationProblemDetails(
                HttpContext,
                ModelState);

        validationProblemDetails.Detail =
            "See the errors field for details.";
        validationProblemDetails.Instance =
            HttpContext.Request.Path;

        validationProblemDetails.Type =
            "https://allog.com.br/modelvalidationproblem";
        validationProblemDetails.Status =
            StatusCodes.Status422UnprocessableEntity;
        validationProblemDetails.Title =
            "One or more validation errors occurred.";

        return new UnprocessableEntityObjectResult(
            validationProblemDetails)
        {
            ContentTypes = { "application/problem+json" }
        };
    }

    public void ConfigureModelState(Dictionary<string, string[]> errors)
    {
        foreach (var error in errors)
        {
            string key = error.Key;
            string[] values = error.Value;

            foreach (var value in values)
            {
                ModelState.AddModelError(key, value);
            }
        }
    }
}