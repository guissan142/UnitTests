﻿using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Fatturo.Main.Persistence.DbContexts.Configuration;
using Fatturo.Main.Persistence.DbContexts;

namespace Fatturo.Main.Persistence.Extensions
{
    internal static class StartupHelperExtensions
    {
        public static async Task ResetDatabaseAsync(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                try
                {
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    if (context is null)
                    {
                        await context!.Database.EnsureDeletedAsync();
                        await context.Database.MigrateAsync();
                    }
                }
                catch (Exception ex)
                {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger>();
                    logger.LogError(ex, "An error occurred while migrating the database.");
                }
            }
        }
    }
}
