// dotnet ef --startup-project ../Fatturo.Main.Api/ migrations add InitialMigration --context ApplicationDbContext
// dotnet ef --startup-project ../Fatturo.Main.Api/ database update --context ApplicationDbContext

using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Extensions;
using Fatturo.Main.Persistence.DbContexts;
using Fatturo.Main.Persistence.Repositories;
using Fatturo.Main.Persistence.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddPersistenceServices(builder.Configuration); // Invers�o de Depend�ncia

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddApplicationServices();




var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//await app.ResetDatabaseAsync();

app.Run();

public partial class Program { }
