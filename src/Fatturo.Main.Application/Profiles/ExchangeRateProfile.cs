﻿
using AutoMapper;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Domain.Entities;

namespace Fatturo.Main.Application.Profiles
{
    public class ExchangeRateProfile : Profile
    {
        public ExchangeRateProfile() 
        {
            CreateMap<CreateExchangeRatesCommand, ExchangeRate>()
                .ForAllMembers(
                (cfg) => cfg.MapFrom(
                        c => new ExchangeRate(
                                c.QuotationDate,
                                c.Factor,
                                c.TypeId,
                                c.CurrencyOriginId,
                                c.CurrencyDestinationId
                        )
                    )
                );

            CreateMap<UpdateExchangeRatesCommand, ExchangeRate>()
                .ForAllMembers(
                    cfg => cfg.MapFrom(
                         /*(u, e) => e.Update(
                             u.QuotationDate,
                             u.Factor,
                             u.TypeId,
                             u.CurrencyOriginId,
                             u.CurrencyDestinationId
                         )*/
                    )
                );
        }
    }
}
