﻿

using AutoMapper;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Domain.Entities;

namespace Fatturo.Main.Application.Profiles
{
    public class ExchangeRatesProfile : Profile
    {
        public ExchangeRatesProfile() 
        {
            CreateMap<ExchangeRate, CreateExchangeRate>()
                .ForMember(
                    d => d.Type,
                    o => o.MapFrom(e => e.Type.ToString()));

            CreateMap<ExchangeRate, ExchangeRateDto>()
                .ForMember(
                    d => d.Type,
                    o => o.MapFrom(e => e.Type.ToString()));
        }
    }
}
