﻿
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Fatturo.Main.Application.Extensions
{
    public static class ApplicationServiceRegistration
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services) 
        { 
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

            services.AddFluentValidationServices();

            return services;
        } 

        public static void AddFluentValidationServices(this IServiceCollection services)
        {
            services.AddScoped<IValidator<CreateExchangeRateCommand>, CreateExchangeRatesCommandValidator>();
            services.AddScoped<IValidator<UpdateExchangeRateCommand>, UpdateExchangeRatesCommandValidator>();
            services.AddScoped<IValidator<DeleteExchangeRatesCommand>, DeleteExchangeRatesCommandValidator>();
            services.AddScoped<IValidator<GetExchangeRateByIdQuery>, GetExchangeRatesByIdQueryValidator>();
            services.AddScoped<IValidator<GetExchangeRatesByMacroFilterQuery>, GetExchangeRatesByMacroFilterQueryValidator>();
        }
    }
}
