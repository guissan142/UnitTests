using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter;
using Fatturo.Main.Domain.Entities;

namespace Fatturo.Main.Application.Contracts
{
    public interface IExchangeRateRepository
    {
        Task<bool> SaveChangesAsync();

        void AddExchangeRate(ExchangeRate exchangeRate);

        void DeleteExchangeRate(ExchangeRate exchangeRate);

        Task<bool> ExchangeRateExistsAsync(CreateExchangeRateCommand exchangeRate);

        Task<bool> SameExchangeRateDataExistsAsync(UpdateExchangeRateCommand exchangeRate);

        Task<ExchangeRate?> GetExchangeRateByIdAsync(Guid id);

        Task<IEnumerable<ExchangeRate>> GetExchangeRatesByMacroFilterAsync(
            GetExchangeRatesByMacroFilterQuery getExchangeRatesByMacroFilterQuery
        );

        Task<bool> CurrencyExistsAsync(Guid currencyId);
    }
}
