﻿using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById
{
    public record GetExchangeRateByIdQuery(Guid Id) : IRequest<GetExchangeRateByIdQueryResponse>
    {
    }
}
