﻿namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById
{
    public class ExchangeRateDto
    {
        public Guid Id { get; set; }
        public DateOnly QuotationDate { get; set; }
        public decimal Factor { get; set; }
        public string Type { get; set; } = string.Empty;
        public Guid CurrencyOriginId { get; set; }
        public Guid CurrencyDestinationId { get; set; }
    }
}
