﻿using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using FluentValidation.Results;
using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById
{
    public class GetExchangeRateByIdQueryHandler 
        : IRequestHandler<GetExchangeRateByIdQuery, GetExchangeRateByIdQueryResponse>
    {
        public readonly IMapper _mapper;

        public readonly IExchangeRateRepository _exchangeRateRepository;

        public GetExchangeRateByIdQueryHandler(
            IMapper mapper, 
            IExchangeRateRepository exchangeRateRepository) 
        {
            _mapper = mapper;
            _exchangeRateRepository = exchangeRateRepository;
        }

        public async Task<GetExchangeRateByIdQueryResponse> Handle(
            GetExchangeRateByIdQuery request, 
            CancellationToken cancellationToken)
        {
            var getExchangeRatesByIdQueryResponse = new GetExchangeRateByIdQueryResponse();

            var validator = new GetExchangeRatesByIdQueryValidator();
            var validationResult = await validator.ValidateAsync(request);

            if (!validationResult.IsValid)
            {
                getExchangeRatesByIdQueryResponse.AddErrors(validationResult, Error.ValidationProblem);
                return getExchangeRatesByIdQueryResponse;
            }

            var exchangeRatesFromDatabase = await _exchangeRateRepository
                                                        .GetExchangeRateByIdAsync(request.Id);

            if (exchangeRatesFromDatabase! == null!)
            {
                validationResult.Errors.Add(new ValidationFailure(
                                                "ExchangeRate", 
                                                "ExchangeRate Not Found"));

                getExchangeRatesByIdQueryResponse.AddErrors(validationResult, Error.NotFoundProblem);
                return getExchangeRatesByIdQueryResponse;
            }

            getExchangeRatesByIdQueryResponse.ExchangeRate = _mapper
                .Map<ExchangeRateDto>(exchangeRatesFromDatabase);

            return getExchangeRatesByIdQueryResponse;
        }
    }
}
