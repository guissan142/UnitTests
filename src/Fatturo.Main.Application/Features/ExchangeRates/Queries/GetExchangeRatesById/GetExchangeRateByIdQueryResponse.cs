﻿using Fatturo.Main.Application.Features.Common;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById
{
    public class GetExchangeRateByIdQueryResponse : BaseResponse
    {
        public ExchangeRateDto ExchangeRate { get; set; } = default!;
    }
}
