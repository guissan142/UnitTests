using FluentValidation;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;

public class GetExchangeRatesByIdQueryValidator : AbstractValidator<GetExchangeRateByIdQuery>
{
    public GetExchangeRatesByIdQueryValidator()
    {
        RuleFor(q => q.Id)
            .NotEmpty()
            .WithMessage("{PropertyName} is required.");
    }
}