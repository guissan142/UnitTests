﻿
using FluentValidation;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter
{
    public class GetExchangeRatesByMacroFilterQueryValidator : AbstractValidator<GetExchangeRatesByMacroFilterQuery>
    {
    }
}
