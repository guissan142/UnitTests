﻿using Fatturo.Main.Application.Features.Common;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter
{
    public class GetExchangeRatesByMacroFilterQueryResponse : BaseResponse
    {
    }
}
