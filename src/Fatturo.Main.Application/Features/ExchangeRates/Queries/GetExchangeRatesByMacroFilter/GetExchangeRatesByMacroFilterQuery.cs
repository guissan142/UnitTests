using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesByMacroFilter;

public class GetExchangeRatesByMacroFilterQuery : IRequest<GetExchangeRatesByMacroFilterQueryResponse>
{
    public DateOnly quotationDateStart { get; set; }
    public DateOnly quotationDateEnd { get; set; }
    public List<int> type { get; set; } = new();
    public Guid? currencyOriginId { get; set; }
    public Guid? currencyDestinationId { get; set; }
}