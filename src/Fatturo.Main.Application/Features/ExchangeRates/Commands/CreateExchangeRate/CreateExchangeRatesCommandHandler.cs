﻿using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates
{
    public class CreateExchangeRatesCommandHandler : 
        IRequestHandler<CreateExchangeRateCommand, CreateExchangeRateCommandResponse>
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;

        private readonly IMapper _mapper;


        public CreateExchangeRatesCommandHandler(
            IExchangeRateRepository exchangeRateRepository,
            IMapper mapper)
        {
            _exchangeRateRepository = exchangeRateRepository;
            _mapper = mapper;
        }

        public async Task<CreateExchangeRateCommandResponse> Handle(
            CreateExchangeRateCommand request,
            CancellationToken cancellationToken)
        {
            var createExchangeRateCommandResponse = new CreateExchangeRateCommandResponse();
            var validator = new CreateExchangeRatesCommandValidator();
            var validationResult = await validator.ValidateAsync(request);

            if(!validationResult.IsValid)
            {
                createExchangeRateCommandResponse.AddErrors(validationResult, Error.ValidationProblem);
                return createExchangeRateCommandResponse;
            }

            if(await _exchangeRateRepository.ExchangeRateExistsAsync(request))
            {
                validationResult.Errors.Add(
                    new ValidationFailure("ExchangeRate", "ExchangeRate Already Exists")
                );
                createExchangeRateCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
                return createExchangeRateCommandResponse;
            }

            if(!await _exchangeRateRepository.CurrencyExistsAsync(request.CurrencyOriginId))
            {
                validationResult.Errors.Add(
                    new ValidationFailure("Currency", "CurrencyOrigin with the Id provided does not exist")
                );
                createExchangeRateCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
                return createExchangeRateCommandResponse;
            }

            if(!await _exchangeRateRepository.CurrencyExistsAsync(request.CurrencyDestinationId))
            {
                validationResult.Errors.Add(
                    new ValidationFailure("Currency", "CurrencyDestination with the Id provided does not exist")
                );
                createExchangeRateCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
                return createExchangeRateCommandResponse;
            }

            var exchangeRateEntity = new ExchangeRate(
                    request.QuotationDate,
                    request.Factor,
                    request.Type,
                    request.CurrencyOriginId,
                    request.CurrencyDestinationId
            );

            _exchangeRateRepository.AddExchangeRate(exchangeRateEntity);
            
            await _exchangeRateRepository.SaveChangesAsync();

            createExchangeRateCommandResponse.ExchangeRate = _mapper.Map<CreateExchangeRate>(exchangeRateEntity);
            
            return createExchangeRateCommandResponse;
        }
    }
}
