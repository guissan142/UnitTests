﻿
using Fatturo.Main.Domain.Entities;
using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates
{
    public record CreateExchangeRateCommand : IRequest<CreateExchangeRateCommandResponse>
    {
        public DateOnly QuotationDate { get; set; }
        public decimal Factor { get; set; }
        public string Type { get; set; } = string.Empty;
        public Guid CurrencyOriginId { get; set; }
        public Guid CurrencyDestinationId { get; set; }
    }
}
