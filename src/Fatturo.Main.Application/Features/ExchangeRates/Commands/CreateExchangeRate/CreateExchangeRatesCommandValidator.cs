﻿using FluentValidation;
using Fatturo.Main.Domain.Entities;
using Fatturo.Main.Domain.Utils;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates
{
    public class CreateExchangeRatesCommandValidator : AbstractValidator<CreateExchangeRateCommand>
    {
        public CreateExchangeRatesCommandValidator() 
        {
            RuleFor(exchangeRate => exchangeRate.QuotationDate) 
                .NotEmpty() 
                .WithMessage("{PropertyName} is required.")
                .LessThanOrEqualTo(DateOnly.FromDateTime(DateTime.Now)) 
                .WithMessage("{PropertyName} must not be in the future.");

            RuleFor(exchangeRate => exchangeRate.Factor)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.")
                .GreaterThan(0M)
                .WithMessage("{PropertyName} should be greater than 0.");

            RuleFor(exchangeRate => exchangeRate.Type)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.")
                .Must(EnumUtils.IsInEnum<ExchangeRateType>)
                .WithMessage("{PropertyName} must be Valid.");

            RuleFor(exchangeRate => exchangeRate.CurrencyOriginId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.")
                .NotEqual(exchangeRate => exchangeRate.CurrencyDestinationId)
                .WithMessage("{PropertyName} and Currency Destination Id must not be the same.");

            RuleFor(exchangeRate => exchangeRate.CurrencyDestinationId)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");
        }
    }
}
