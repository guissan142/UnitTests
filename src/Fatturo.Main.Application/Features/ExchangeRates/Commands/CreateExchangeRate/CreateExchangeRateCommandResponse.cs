﻿using Fatturo.Main.Application.Features.Common;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates
{
    public class CreateExchangeRateCommandResponse : BaseResponse
    {
        public CreateExchangeRate ExchangeRate { get; set; } = default!;
    }
}
