using System.ComponentModel.DataAnnotations;
using Fatturo.Main.Application.Features.Common;
using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;

public record UpdateExchangeRateCommand(Guid Id) : IRequest<UpdateExchangeRatesCommandResponse>
{
    public DateOnly QuotationDate { get; set; }
    public decimal Factor { get; set; }
    public string Type { get; set; } = string.Empty;
    public Guid CurrencyOriginId { get; set; }
    public Guid CurrencyDestinationId { get; set; }
}