using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;

public class UpdateExchangeRatesCommandHandler 
    : IRequestHandler<UpdateExchangeRateCommand, UpdateExchangeRatesCommandResponse>
{
    private readonly IExchangeRateRepository _exchangeRateRepository;
    
    public UpdateExchangeRatesCommandHandler(IExchangeRateRepository exchangeRateRepository)
    {
        _exchangeRateRepository = exchangeRateRepository;
    }

    public async Task<UpdateExchangeRatesCommandResponse> Handle(UpdateExchangeRateCommand request, CancellationToken cancellationToken)
    {
        var updateExchangeRatesCommandResponse = new UpdateExchangeRatesCommandResponse();

        var validator = new UpdateExchangeRatesCommandValidator();
        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
        {
            updateExchangeRatesCommandResponse.AddErrors(validationResult, Error.ValidationProblem);
            return updateExchangeRatesCommandResponse;
        }

        if (!await _exchangeRateRepository.CurrencyExistsAsync(request.CurrencyOriginId))
        {
            validationResult.Errors.Add(
                new ValidationFailure("Currency", "CurrencyOrigin with the Id provided does not exist")
            );
            updateExchangeRatesCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
            return updateExchangeRatesCommandResponse;
        }

        if (!await _exchangeRateRepository.CurrencyExistsAsync(request.CurrencyDestinationId))
        {
            validationResult.Errors.Add(
                new ValidationFailure("Currency", "CurrencyDestination with the Id provided does not exist")
            );
            updateExchangeRatesCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
            return updateExchangeRatesCommandResponse;
        }

        var exchangeRatesFromDatabase = await _exchangeRateRepository.GetExchangeRateByIdAsync(request.Id);

        if (exchangeRatesFromDatabase is null)
        {
            validationResult.Errors.Add(new ValidationFailure("ExchangeRate", "ExchangeRate Not Found"));
            updateExchangeRatesCommandResponse.AddErrors(validationResult, Error.NotFoundProblem);
            return updateExchangeRatesCommandResponse;
        }
        
        if (await _exchangeRateRepository.SameExchangeRateDataExistsAsync(request))
        {
            validationResult.Errors.Add(new ValidationFailure("ExchangeRate", "ExchangeRate Already Exists"));
            updateExchangeRatesCommandResponse.AddErrors(validationResult, Error.BadRequestProblem);
            return updateExchangeRatesCommandResponse;
        }

        exchangeRatesFromDatabase.Update(
            request.QuotationDate,
            request.Factor,
            request.Type,
            request.CurrencyOriginId,
            request.CurrencyDestinationId);


        await _exchangeRateRepository.SaveChangesAsync();

        return updateExchangeRatesCommandResponse;
    }
}