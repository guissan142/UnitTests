﻿using Fatturo.Main.Application.Contracts;
using MediatR;
using FluentValidation;
using FluentValidation.Results;
using Fatturo.Main.Application.Features.Common;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates
{
    public class DeleteExchangeRatesCommandHandler 
        : IRequestHandler<DeleteExchangeRatesCommand, DeleteExchangeRatesCommandResponse>
    {
        public readonly IExchangeRateRepository _exchangeRatesRepository;

        public DeleteExchangeRatesCommandHandler(IExchangeRateRepository exchangeRatesRepository) 
        { 
            _exchangeRatesRepository = exchangeRatesRepository;
        }

        public async Task<DeleteExchangeRatesCommandResponse> Handle(
            DeleteExchangeRatesCommand request, 
            CancellationToken cancellationToken)
        {
            var deleteExchangeRatesCommandResponse = new DeleteExchangeRatesCommandResponse();

            var validator = new DeleteExchangeRatesCommandValidator();
            var validationResult = await validator.ValidateAsync(request);

            if (!validationResult.IsValid)
            {
                deleteExchangeRatesCommandResponse.AddErrors(validationResult, Error.ValidationProblem);
                return deleteExchangeRatesCommandResponse;
            }

            var exchangeFromDatabase = await _exchangeRatesRepository.GetExchangeRateByIdAsync(request.Id);

            if (exchangeFromDatabase is null)
            {
                validationResult.Errors.Add(new ValidationFailure("ExchangeRate", "ExchangeRate Not Found"));
                deleteExchangeRatesCommandResponse.AddErrors(validationResult, Error.NotFoundProblem);
                return deleteExchangeRatesCommandResponse;
            }

            _exchangeRatesRepository.DeleteExchangeRate(exchangeFromDatabase);

            await _exchangeRatesRepository.SaveChangesAsync();

            return deleteExchangeRatesCommandResponse;
        }
    }
}
