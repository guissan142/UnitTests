﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates
{
    public record DeleteExchangeRatesCommand(Guid Id) : IRequest<DeleteExchangeRatesCommandResponse>
    {
    }
}
