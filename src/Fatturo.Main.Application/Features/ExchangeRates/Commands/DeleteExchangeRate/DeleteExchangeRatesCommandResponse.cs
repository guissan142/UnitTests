﻿using Fatturo.Main.Application.Features.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates
{
    public class DeleteExchangeRatesCommandResponse : BaseResponse
    {
    }
}
