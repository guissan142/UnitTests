﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates
{
    public class DeleteExchangeRatesCommandValidator : AbstractValidator<DeleteExchangeRatesCommand>
    {
        public DeleteExchangeRatesCommandValidator()
        {
            RuleFor(c => c.Id)
                .NotEmpty()
                .WithMessage("{PropertyName} is required.");

        }
    }
}
