﻿using FluentValidation.Results;

namespace Fatturo.Main.Application.Features.Common
{
    public enum Error { ValidationProblem, NotFoundProblem, BadRequestProblem }

    public class BaseResponse
    {
        public bool IsSuccess
        {
            get { return Errors.Count == 0; }
        }

        public Dictionary<string, string[]> Errors { get; set; } = new();

        public Error? ErrorType { get; set; } = null;

        private void FillErrors(ValidationResult validationResult)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                Errors.Add(error.Key, error.Value);
            }
        }

        private void FillErrorType(Error errorType)
        {
            ErrorType = errorType;
        }

        public void AddErrors(ValidationResult validationResult, Error errorType)
        {
            FillErrors(validationResult);
            FillErrorType(errorType);
        }
    }
}
