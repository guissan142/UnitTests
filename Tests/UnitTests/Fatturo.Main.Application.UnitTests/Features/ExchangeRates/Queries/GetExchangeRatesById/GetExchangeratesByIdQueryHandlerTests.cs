using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Application.UnitTests.TestUtils;
using Fatturo.Main.Domain.Entities;
using FluentAssertions;
using Moq;

namespace Fatturo.Main.Application.UnitTests.Features.ExchangeRates.Queries.GetExchangeRatesById;

[Collection(nameof(ExchangeRatesTestsCollection))]
public class GetExchangeRatesByIdQueryHandlerTests : IClassFixture<ExchangeRatesDataGeneratingFixture>
{
    private readonly ExchangeRatesDataGeneratingFixture _dataFixture;
    private readonly ExchangeRatesTestsCollectionFixture _configFixture;
    private readonly GetExchangeRateByIdQueryHandler _handler;
    private readonly Mock<IExchangeRateRepository> _repo;
    private readonly Mock<IMapper> _mapper;

    public GetExchangeRatesByIdQueryHandlerTests(ExchangeRatesDataGeneratingFixture dataFixture, ExchangeRatesTestsCollectionFixture configFixture)
    {
        _dataFixture = dataFixture;
        _configFixture = configFixture;
        _repo = _configFixture.GenerateAndSetupExchangeRateRepository();
        _mapper = _configFixture.GenerateAndSetupMapper();
        _handler = _configFixture.GenerateGetExchangeRatesByIdQueryHandler(_repo.Object, _mapper.Object);
    }
    
    [Fact(DisplayName = "When Query Is Valid And Validation Passes : Should Return Response With No Errors")]
    [Trait("Category", "Get Exchange Rates By Id Query Handler Tests")]
    public async Task Handle_WhenQueryIsValidAndValidationPasses_ShouldReturnResponseWithNoErrors()
    {
        var command = _dataFixture.GenerateValidGetExchangeRatesByIdQuery();

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeTrue();
        response.Errors.Should().BeEmpty();
    }
    
    [Fact(DisplayName = "When Query Is Not Valid : Should Return Response With Correct Errors")]
    [Trait("Category", "Get Exchange Rates By Id Query Handler Tests")]
    public async Task Handle_WhenQueryIsNotValid_ShouldReturnResponseWithCorrectErrors()
    {
        var command = _dataFixture.GenerateInvalidGetExchangeRatesByIdQuery();

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeFalse();
        response.ErrorType.Should().Be(Error.ValidationProblem);
        response.Errors.Values.FirstOrDefault().Should().Equal("Id is required.");
    }
    
    [Fact(DisplayName = "When Exchange Rate Is Not In Db : Should Return Response With Correct Errors")]
    [Trait("Category", "Get Exchange Rates By Id Query Handler Tests")]
    public async Task Handle_WhenExchangeRateIsNotInDb_ShouldReturnResponseWithCorrectErrors()
    {
        var command = _dataFixture.GenerateValidGetExchangeRatesByIdQuery();
        _repo.Setup(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>())).ReturnsAsync((ExchangeRate)null!);

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeFalse();
        response.ErrorType.Should().Be(Error.NotFoundProblem);
        response.Errors.Values.FirstOrDefault().Should().Equal("ExchangeRate Not Found");
    }
    
    [Fact(DisplayName = "When Query Is Valid And Validation Passes : Should Execute Dependencies Correctly")]
    [Trait("Category", "Get Exchange Rates By Id Query Handler Tests")]
    public async Task Handle_WhenQueryIsValidAndValidationPasses_ShouldExecuteDependenciesCorrectly()
    {
        var command = _dataFixture.GenerateValidGetExchangeRatesByIdQuery();

        var response = await _handler.Handle(command, default);

        _repo.Verify(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>()), Times.Once);
    }
}

