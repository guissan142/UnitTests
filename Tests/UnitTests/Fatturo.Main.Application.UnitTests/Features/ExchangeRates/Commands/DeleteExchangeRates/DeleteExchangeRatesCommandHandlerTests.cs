using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.UnitTests.TestUtils;
using Fatturo.Main.Domain.Entities;
using FluentAssertions;
using Moq;

namespace Fatturo.Main.Application.UnitTests.Features.ExchangeRates.Commands.DeleteExchangeRates;

[Collection(nameof(ExchangeRatesTestsCollection))]
public class DeleteExchangeRatesCommandHandlerTests : IClassFixture<ExchangeRatesDataGeneratingFixture>
{
    private readonly ExchangeRatesDataGeneratingFixture _dataFixture;
    private readonly ExchangeRatesTestsCollectionFixture _configFixture;
    private readonly DeleteExchangeRatesCommandHandler _handler;
    private readonly Mock<IExchangeRateRepository> _repo;
    
    public DeleteExchangeRatesCommandHandlerTests(ExchangeRatesDataGeneratingFixture dataFixture, ExchangeRatesTestsCollectionFixture configFixture)
    {
        _dataFixture = dataFixture;
        _configFixture = configFixture;
        _repo = _configFixture.GenerateAndSetupExchangeRateRepository();
        _handler = _configFixture.GenerateDeleteExchangeRatesCommandHandler(_repo.Object);
    }
    
    [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Return Response With No Errors")]
    [Trait("Category", "Delete Exchange Rates Command Handler Tests")]
    public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldReturnResponseWithNoErrors()
    {
        var command = _dataFixture.GenerateValidDeleteExchangeRatesCommand();

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeTrue();
        response.Errors.Should().BeEmpty();
    }
    
    [Fact(DisplayName = "When Command Is Not Valid : Should Return Response With Correct Errors")]
    [Trait("Category", "Delete Exchange Rates Command Handler Tests")]
    public async Task Handle_WhenCommandIsNotValid_ShouldReturnResponseWithCorrectErrors()
    {
        var command = _dataFixture.GenerateInvalidDeleteExchangeRatesCommand();

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeFalse();
        response.ErrorType.Should().Be(Error.ValidationProblem);
        response.Errors.Values.FirstOrDefault().Should().Equal("Id is required.");
    }
    
    [Fact(DisplayName = "When Exchange Rate Is Not In Db : Should Return Response With Correct Errors")]
    [Trait("Category", "Delete Exchange Rates Command Handler Tests")]
    public async Task Handle_WhenExchangeRateIsNotInDb_ShouldReturnResponseWithCorrectErrors()
    {
        var command = _dataFixture.GenerateValidDeleteExchangeRatesCommand();
        _repo.Setup(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>())).ReturnsAsync((ExchangeRate)null!);

        var response = await _handler.Handle(command, default);

        response.IsSuccess.Should().BeFalse();
        response.ErrorType.Should().Be(Error.NotFoundProblem);
        response.Errors.Values.FirstOrDefault().Should().Equal("ExchangeRate Not Found");
    }
    
    [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Execute Dependencies Correctly")]
    [Trait("Category", "Delete Exchange Rates Command Handler Tests")]
    public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldExecuteDependenciesCorrectly()
    {
        var command = _dataFixture.GenerateValidDeleteExchangeRatesCommand();

        var response = await _handler.Handle(command, default);

        _repo.Verify(r => r.SaveChangesAsync(), Times.Once);
        _repo.Verify(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>()), Times.Once);
        _repo.Verify(r => r.DeleteExchangeRate(It.IsAny<ExchangeRate>()), Times.Once);
    }
}