﻿using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.UnitTests.TestUtils;
using Fatturo.Main.Domain.Entities;
using FluentAssertions;
using FluentValidation;
using Moq;

namespace Fatturo.Main.Application.UnitTests.Features.ExchangeRates.Commands.UpdateExchangeRates
{
    [Collection(nameof(ExchangeRatesTestsCollection))]
    public class UpdateExchangeRatesCommandHandlerTests : IClassFixture<ExchangeRatesDataGeneratingFixture>
    {
        private readonly ExchangeRatesDataGeneratingFixture _dataFixture;
        private readonly ExchangeRatesTestsCollectionFixture _configFixture;
        private readonly UpdateExchangeRatesCommandHandler _handler;
        private readonly Mock<IExchangeRateRepository> _repo;

        public UpdateExchangeRatesCommandHandlerTests(ExchangeRatesDataGeneratingFixture dataFixture, ExchangeRatesTestsCollectionFixture configFixture)
        {
            _dataFixture = dataFixture;
            _configFixture = configFixture;
            _repo = _configFixture.GenerateAndSetupExchangeRateRepository();
            _handler = _configFixture.GenerateUpdateExchangeRatesCommandHandler(_repo.Object);
        }

        [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Return Response With No Errors")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldReturnResponseWithNoErrors()
        {
            
        }
        // var command = _dataFixture.GenerateValidUpdateExchangeRatesCommand();

        //     var response = await _handler.Handle(command, default);

        //     response.Should().BeOfType<UpdateExchangeRatesCommandResponse>();
        //     response.IsSuccess.Should().BeTrue();
        //     response.Errors.Should().BeEmpty();

        [Fact(DisplayName = "When Command Is Not Valid : Should Return Response With Correct Errors")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsNotValid_ShouldReturnResponseWithCorrectErrors()
        {
            
        }
        // var command = _dataFixture.GenerateInvalidUpdateExchangeRatesCommand();

        //     var response = await _handler.Handle(command, default);

        //     response.IsSuccess.Should().BeFalse();
        //     response.ErrorType.Should().Be(Error.ValidationProblem);
        //     response.Errors.Values.FirstOrDefault().Should().Equal("Quotation Date must not be in the future.");

        [Fact(DisplayName = "When Same Exchange Rate Already Exists : Should Return Response With Correct Errors")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenSameExchangeRateAlreadyExists_ShouldReturnResponseWithCorrectErrors()
        {
            
        }
        // var command = _dataFixture.GenerateValidUpdateExchangeRatesCommand();
        //     _repo.Setup(r => r.SameExchangeRateDataExistsAsync(It.IsAny<UpdateExchangeRateCommand>())).ReturnsAsync(true);

        //     var response = await _handler.Handle(command, default);

        //     response.IsSuccess.Should().BeFalse();
        //     response.ErrorType.Should().Be(Error.BadRequestProblem);
        //     response.Errors.Values.FirstOrDefault().Should().Equal("ExchangeRate Already Exists");

        [Fact(DisplayName = "When Exchange Rate Is Not In Db : Should Return Response With Correct Errors")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenExchangeRateIsNotInDb_ShouldReturnResponseWithCorrectErrors()
        {
            
        }
        // var command = _dataFixture.GenerateValidUpdateExchangeRatesCommand();
        //     _repo.Setup(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>())).ReturnsAsync((ExchangeRate)null!);

        //     var response = await _handler.Handle(command, default);

        //     response.IsSuccess.Should().BeFalse();
        //     response.ErrorType.Should().Be(Error.NotFoundProblem);
        //     response.Errors.Values.FirstOrDefault().Should().Equal("ExchangeRate Not Found");
        
        
        [Fact(DisplayName = "When Currency Does Not Exist In Db : Should Return Response With Correct Errors")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCurrencyDoesNotExistInDb_ShouldReturnResponseWithCorrectErrors()
        {
            var command = _dataFixture.GenerateValidUpdateExchangeRatesCommand();
            _repo.Setup(r => r.CurrencyExistsAsync(It.IsAny<Guid>())).ReturnsAsync(false);

            var response = await _handler.Handle(command, default);

            response.IsSuccess.Should().BeFalse();
            response.ErrorType.Should().Be(Error.BadRequestProblem);
            response.Errors.Values.FirstOrDefault().Should().Equal("CurrencyOrigin with the Id provided does not exist");
        }

        [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Execute Dependencies Correctly")]
        [Trait("Category", "Update Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldExecuteDependenciesCorrectly()
        {
            var command = _dataFixture.GenerateValidUpdateExchangeRatesCommand();

            var response = await _handler.Handle(command, default);

            _repo.Verify(r => r.SaveChangesAsync(), Times.Once);
            _repo.Verify(r => r.GetExchangeRateByIdAsync(It.IsAny<Guid>()), Times.Once);
            _repo.Verify(r => r.SameExchangeRateDataExistsAsync(It.IsAny<UpdateExchangeRateCommand>()), Times.Once);
            _repo.Verify(r => r.CurrencyExistsAsync(It.IsAny<Guid>()), Times.Exactly(2));

        }








    }
}
