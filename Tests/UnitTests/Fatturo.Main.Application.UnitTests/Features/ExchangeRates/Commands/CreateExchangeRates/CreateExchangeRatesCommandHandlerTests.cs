﻿using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.UnitTests.TestUtils;
using Fatturo.Main.Domain.Entities;
using FluentAssertions;
using FluentValidation;
using Moq;

namespace Fatturo.Main.Application.UnitTests.Features.ExchangeRates.Commands.CreateExchangeRates
{
    [Collection(nameof(ExchangeRatesTestsCollection))]
    public class CreateExchangeRatesCommandHandlerTests : IClassFixture<ExchangeRatesDataGeneratingFixture>
    {
        private readonly ExchangeRatesDataGeneratingFixture _dataFixture;
        private readonly ExchangeRatesTestsCollectionFixture _configFixture;
        private readonly CreateExchangeRatesCommandHandler _handler;
        private readonly Mock<IExchangeRateRepository> _repo;
        private readonly Mock<IMapper> _mapper;

        public CreateExchangeRatesCommandHandlerTests(ExchangeRatesDataGeneratingFixture dataFixture, ExchangeRatesTestsCollectionFixture configFixture)
        {
            _dataFixture = dataFixture;
            _configFixture = configFixture;
            _repo = _configFixture.GenerateAndSetupExchangeRateRepository();
            _mapper = _configFixture.GenerateAndSetupMapper();
            _handler = _configFixture.GenerateCreateExchangeRatesCommandHandler(_repo.Object, _mapper.Object);
        }

        [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Return Response With No Errors")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldReturnResponseWithNoErrors()
        {
            var command = _dataFixture.GenerateValidCreateExchangeRatesCommand();

            var response = await _handler.Handle(command, default);

            response.IsSuccess.Should().BeTrue();
            response.Errors.Should().BeEmpty();
        }

        [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Return Response With Valid Exchange Rate")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldReturnResponseWithValidExchangeRate()
        {
            var command = _dataFixture.GenerateValidCreateExchangeRatesCommand();

            var response = await _handler.Handle(command, default);

            response.Should().BeOfType<CreateExchangeRateCommandResponse>();
            response.ExchangeRate.Should().BeOfType<CreateExchangeRate>();
            response.ExchangeRate.Should().NotBeNull();
        }

        [Fact(DisplayName = "When Command Is Not Valid : Should Return Response With Correct Errors")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsNotValid_ShouldReturnResponseWithCorrectErrors()
        {
            var command = _dataFixture.GenerateInvalidCreateExchangeRatesCommand();

            var response = await _handler.Handle(command, default);

            response.IsSuccess.Should().BeFalse();
            response.ErrorType.Should().Be(Error.ValidationProblem);
            response.Errors.Values.FirstOrDefault().Should().Equal("Quotation Date must not be in the future.");
        }

        [Fact(DisplayName = "When Same Exchange Rate Already Exists : Should Return Response With Correct Errors")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenSameExchangeRateAlreadyExists_ShouldReturnResponseWithCorrectErrors()
        {
            var command = _dataFixture.GenerateValidCreateExchangeRatesCommand();
            _repo.Setup(r => r.ExchangeRateExistsAsync(It.IsAny<CreateExchangeRateCommand>())).ReturnsAsync(true);

            var response = await _handler.Handle(command, default);

            response.IsSuccess.Should().BeFalse();
            response.ErrorType.Should().Be(Error.BadRequestProblem);
            response.Errors.Values.FirstOrDefault().Should().Equal("ExchangeRate Already Exists");
        }
        
        [Fact(DisplayName = "When Currency Does Not Exist In Db : Should Return Response With Correct Errors")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCurrencyDoesNotExistInDb_ShouldReturnResponseWithCorrectErrors()
        {
            var command = _dataFixture.GenerateValidCreateExchangeRatesCommand();
            _repo.Setup(r => r.CurrencyExistsAsync(It.IsAny<Guid>())).ReturnsAsync(false);

            var response = await _handler.Handle(command, default);

            response.IsSuccess.Should().BeFalse();
            response.ErrorType.Should().Be(Error.BadRequestProblem);
            response.Errors.Values.FirstOrDefault().Should().Equal("CurrencyOrigin with the Id provided does not exist");
        }

        [Fact(DisplayName = "When Command Is Valid And Validation Passes : Should Execute Dependencies Correctly")]
        [Trait("Category", "Create Exchange Rates Command Handler Tests")]
        public async Task Handle_WhenCommandIsValidAndValidationPasses_ShouldExecuteDependenciesCorrectly()
        {
            var command = _dataFixture.GenerateValidCreateExchangeRatesCommand();

            var response = await _handler.Handle(command, default);

            _repo.Verify(r => r.SaveChangesAsync(), Times.Once);
            _repo.Verify(r => r.AddExchangeRate(It.IsAny<ExchangeRate>()), Times.Once);
            _repo.Verify(r => r.ExchangeRateExistsAsync(It.IsAny<CreateExchangeRateCommand>()), Times.Once);
            _repo.Verify(r => r.CurrencyExistsAsync(It.IsAny<Guid>()), Times.Exactly(2));
            _mapper.Verify(m => m.Map<CreateExchangeRate>(It.IsAny<ExchangeRate>()), Times.Once);
            
        }

    }
}
