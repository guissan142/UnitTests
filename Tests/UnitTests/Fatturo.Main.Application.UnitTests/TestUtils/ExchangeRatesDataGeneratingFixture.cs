﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Domain.Entities;
using FluentValidation;
using FluentValidation.Results;

namespace Fatturo.Main.Application.UnitTests.TestUtils
{
    public class ExchangeRatesDataGeneratingFixture
    {
        public UpdateExchangeRateCommand GenerateValidUpdateExchangeRatesCommand()
        {
            return new UpdateExchangeRateCommand(Guid.NewGuid())
            {
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                Type = "1"
            };
        }
        public UpdateExchangeRateCommand GenerateInvalidUpdateExchangeRatesCommand()
        {
            return new UpdateExchangeRateCommand(Guid.NewGuid())
            {
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
                Type = "4"
            };
        }

        public ExchangeRate GenerateValidExchangeRates()
        {
            return new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                23.500009M,
                "Open",
                Guid.NewGuid(),
                Guid.NewGuid()
            );
        }

        public CreateExchangeRateCommand GenerateValidCreateExchangeRatesCommand()
        {
            return new CreateExchangeRateCommand
            {
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                Type = "1"
            };
        }

        public CreateExchangeRateCommand GenerateInvalidCreateExchangeRatesCommand()
        {
            return new CreateExchangeRateCommand
            {
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
                Type = "1"
            };
        }

        public CreateExchangeRate GenerateValidCreateExchangeRateReturnDto()
        {
            return  new CreateExchangeRate
            {
                Id = Guid.NewGuid(),
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
                Type = ExchangeRateType.Ptax.ToString(),
            };
        }


        public DeleteExchangeRatesCommand GenerateValidDeleteExchangeRatesCommand()
        {
            return new DeleteExchangeRatesCommand(Guid.NewGuid());
        }
        
        public DeleteExchangeRatesCommand GenerateInvalidDeleteExchangeRatesCommand()
        {
            return new DeleteExchangeRatesCommand(Guid.Empty);
        }

        public GetExchangeRateByIdQuery GenerateValidGetExchangeRatesByIdQuery()
        {
            return new GetExchangeRateByIdQuery(Guid.NewGuid());
        }

        public GetExchangeRateByIdQuery GenerateInvalidGetExchangeRatesByIdQuery()
        {
            return new GetExchangeRateByIdQuery(Guid.Empty);
        }
    }
}
