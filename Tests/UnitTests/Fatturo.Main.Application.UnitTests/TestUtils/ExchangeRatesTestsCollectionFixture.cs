﻿using AutoMapper;
using Fatturo.Main.Application.Contracts;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Application.UnitTests.Features.ExchangeRates.Queries.GetExchangeRatesById;
using Fatturo.Main.Domain.Entities;
using FluentValidation;
using FluentValidation.Results;
using Moq;

namespace Fatturo.Main.Application.UnitTests.TestUtils
{
    [CollectionDefinition(nameof(ExchangeRatesTestsCollection))]
    public class ExchangeRatesTestsCollection : ICollectionFixture<ExchangeRatesTestsCollectionFixture>
    {
    }

    public class ExchangeRatesTestsCollectionFixture
    {
        public ExchangeRatesDataGeneratingFixture DataFixture = new();

        public Mock<IExchangeRateRepository> GenerateAndSetupExchangeRateRepository()
        {
            var repo = new Mock<IExchangeRateRepository>();

            repo
                .Setup(x => x
                    .GetExchangeRateByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(DataFixture.GenerateValidExchangeRates())
                .Verifiable();

            repo
                .Setup(r => r
                    .SaveChangesAsync())
                .ReturnsAsync(true)
                .Verifiable();
            
            repo
                .Setup(r => r
                    .ExchangeRateExistsAsync(
                        It.IsAny<CreateExchangeRateCommand>()))
                .ReturnsAsync(false);

            repo
                .Setup(r => r
                    .CurrencyExistsAsync(It.IsAny<Guid>()))
                .ReturnsAsync(true);

            repo
                .Setup(r => r
                    .SameExchangeRateDataExistsAsync(It.IsAny<UpdateExchangeRateCommand>()))
                .ReturnsAsync(false);

            return repo;
        }

        public Mock<IMapper> GenerateAndSetupMapper()
        {
            var mapper = new Mock<IMapper>();

            mapper.Setup(m => m.Map<CreateExchangeRate>(It.IsAny<ExchangeRate>()))
                .Returns(DataFixture.GenerateValidCreateExchangeRateReturnDto());

            return mapper;
        }

        public UpdateExchangeRatesCommandHandler GenerateUpdateExchangeRatesCommandHandler(IExchangeRateRepository repo)
        {
            return new UpdateExchangeRatesCommandHandler(repo);
        }
        public CreateExchangeRatesCommandHandler GenerateCreateExchangeRatesCommandHandler(IExchangeRateRepository repo, IMapper mapper)
        {
            return new CreateExchangeRatesCommandHandler(repo, mapper);
        }

        public DeleteExchangeRatesCommandHandler GenerateDeleteExchangeRatesCommandHandler(IExchangeRateRepository repo)
        {
            return new DeleteExchangeRatesCommandHandler(repo);
        }
        
        public GetExchangeRateByIdQueryHandler GenerateGetExchangeRatesByIdQueryHandler(IExchangeRateRepository repo, IMapper mapper)
        {
            return new GetExchangeRateByIdQueryHandler(mapper, repo);
        }
    }
}
