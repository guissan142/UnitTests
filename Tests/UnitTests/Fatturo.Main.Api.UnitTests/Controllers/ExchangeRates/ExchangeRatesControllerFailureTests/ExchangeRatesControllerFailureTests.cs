﻿using Fatturo.Main.Api.Controllers;
using Fatturo.Main.Api.UnitTests.Controllers.ExchangeRates.ExchangeRatesControllerSuccessfulTests;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq.AutoMock;

namespace Fatturo.Main.Api.UnitTests.Controllers.ExchangeRates.ExchangeRatesControllerFailureTests;

public class ExchangeRatesControllerFailureTests : IClassFixture<ExchangeRatesControllerFailureTestsFixture>
{
    private readonly ExchangeRatesControllerFailureTestsFixture _fixture;
    private readonly ExchangeRatesController _controller;
    private readonly AutoMocker _mocker;

    public ExchangeRatesControllerFailureTests( ExchangeRatesControllerFailureTestsFixture fixture)
    {
        _fixture = fixture;
        _controller = _fixture.GenerateAndSetupExchangeRatesController();
        _mocker = _fixture.Mocker;
    }
    
    [Fact(DisplayName = "Get ExchangeRate By Id : When Query Is Not Successful : Should Return Correct Error")]
    [Trait("Category", "ExchangeRate Controller Failure Tests")]
    [Trait("Method", "GetExchangeRateById")]
    public async void GetExchangeRateById_WhenQueryIsNotSuccessful_ShouldReturnCorrectError()
    {
        var query = _fixture.GenerateInvalidGetExchangeRatesByIdQuery();

        var response = await _controller.GetExchangeRateById(query.Id);

        response.Should().BeOfType<ActionResult<ExchangeRateDto>>();
        response.Result.Should().BeOfType<UnprocessableEntityObjectResult>();
        
    }
    
    [Fact(DisplayName = "Create ExchangeRate : When Command Is Invalid : Should Return Correct Error")]
    [Trait("Category", "ExchangeRate Controller Failure Tests")]
    [Trait("Method", "CreateExchangeRate")]
    public async Task CreateExchangeRate_WhenCommandIsSuccessful_ShouldReturnCreatedAtRouteExchangeRate()
    {
        var command = _fixture.GenerateInvalidCreateExchangeRatesCommand();

        var response = await _controller.CreateExchangeRate(command);
        
        response.Should().BeOfType<ActionResult<CreateExchangeRate>>();
        response.Result.Should().BeOfType<UnprocessableEntityObjectResult>();
    
    }
    
    [Fact(DisplayName = "Update ExchangeRate : When Params Are Invalid : Should Return Bad Request Result")]
    [Trait("Category", "ExchangeRate Controller Failure Tests")]
    [Trait("Method", "UpdateExchangeRate")]
    public async Task UpdateExchangeRate_WhenParamsAreInvalid_ShouldReturnBadRequestResult()
    {
        var command = _fixture.GenerateInvalidUpdateExchangeRatesCommand();

        var response = await _controller.UpdateExchangeRate(Guid.NewGuid(), command);

        response.Should().BeOfType<BadRequestResult>();
    }
    
    [Fact(DisplayName = "Update ExchangeRate : When Command Is Invalid : Should Return Correct Error")]
    [Trait("Category", "ExchangeRate Controller Failure Tests")]
    [Trait("Method", "UpdateExchangeRate")]
    public async Task UpdateExchangeRate_WhenCommandIsInvalid_ShouldReturnCorrectError()
    {
        var command = _fixture.GenerateInvalidUpdateExchangeRatesCommand();

        var response = await _controller.UpdateExchangeRate(command.Id, command);

        response.Should().BeOfType<UnprocessableEntityObjectResult>();
    }
    
    [Fact(DisplayName = "Delete ExchangeRate : When Command Is Invalid  : Should Return Correct Error")]
    [Trait("Category", "ExchangeRate Controller Failure Tests")]
    [Trait("Method", "DeleteExchangeRate")]
    public async Task DeleteExchangeRate_WhenCommandIsInvalid_ShouldReturnCorrectError()
    {
        var command = _fixture.GenerateInvalidDeleteExchangeRatesCommand();
    
        var response = await _controller.DeleteExchangeRate(command.Id);

        response.Should().BeOfType<UnprocessableEntityObjectResult>();
    }
    
}