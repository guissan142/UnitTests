﻿using Fatturo.Main.Api.Controllers;
using Fatturo.Main.Application.Features.Common;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Moq.AutoMock;

namespace Fatturo.Main.Api.UnitTests.Controllers.ExchangeRates.ExchangeRatesControllerFailureTests;

public class ExchangeRatesControllerFailureTestsFixture
{
    public AutoMocker Mocker = null!;
    public ExchangeRatesController Controller = null!;
    
    public ExchangeRatesController GenerateAndSetupExchangeRatesController()
    {
        Mocker = new AutoMocker();
        
        var serviceCollection = new ServiceCollection();
        serviceCollection.AddMvc();
        var serviceProvider = serviceCollection.BuildServiceProvider();
        var httpContext = new DefaultHttpContext { RequestServices = serviceProvider };
        
        Controller = Mocker.CreateInstance<ExchangeRatesController>();
        
        Controller.ControllerContext = new ControllerContext { HttpContext = httpContext };
        
        var mediatorMock = Mocker.GetMock<IMediator>();

        mediatorMock
            .Setup(m => m.Send(It.IsAny<GetExchangeRateByIdQuery>(), default))
            .ReturnsAsync(GenerateInvalidGetExchangeRatesByIdQueryResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<CreateExchangeRateCommand>(), default))
            .ReturnsAsync(GenerateInvalidCreateExchangeRatesCommandResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<DeleteExchangeRatesCommand>(), default))
            .ReturnsAsync(GenerateInvalidDeleteExchangeRatesCommandResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<UpdateExchangeRateCommand>(), default))
            .ReturnsAsync(GenerateInvalidUpdateExchangeRatesCommandResponse);
        
        return Controller;
    }

    // CREATE
    public CreateExchangeRateCommandResponse GenerateInvalidCreateExchangeRatesCommandResponse()
    {
        return new CreateExchangeRateCommandResponse
        {
            ExchangeRate = new CreateExchangeRate
            {
                Id = Guid.NewGuid(),
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
                Type = "Open"
            },
            Errors = new Dictionary<string, string[]>
            {
                { "Error Test", new[] 
                    { "Error Generated In Application Layer"}}
            },
            ErrorType = Error.ValidationProblem
        };
    }

    public CreateExchangeRateCommand GenerateInvalidCreateExchangeRatesCommand()
    {
        return new CreateExchangeRateCommand
        {
            CurrencyDestinationId = Guid.NewGuid(),
            CurrencyOriginId = Guid.NewGuid(),
            Factor = 123.123123M,
            QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
            Type = "4"
        };
    }
    
    
    // UPDATE
    public UpdateExchangeRateCommand GenerateInvalidUpdateExchangeRatesCommand()
    {
        return new UpdateExchangeRateCommand(Guid.NewGuid())
        {
            CurrencyDestinationId = Guid.NewGuid(),
            CurrencyOriginId = Guid.NewGuid(),
            Factor = 123.123123M,
            QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
            Type = "4"
        };
    }
    
    public UpdateExchangeRatesCommandResponse GenerateInvalidUpdateExchangeRatesCommandResponse()
    {
        return new UpdateExchangeRatesCommandResponse()
        {
            Errors = new Dictionary<string, string[]>
            {
                { "Error Test", new[] 
                    { "Error Generated In Application Layer"}}
            },
            ErrorType = Error.ValidationProblem
        };
    }
    
    // DELETE
    
    public DeleteExchangeRatesCommand GenerateInvalidDeleteExchangeRatesCommand()
    {
        return new DeleteExchangeRatesCommand(Guid.Empty);
    }
    
    public DeleteExchangeRatesCommandResponse GenerateInvalidDeleteExchangeRatesCommandResponse()
    {
        return new DeleteExchangeRatesCommandResponse()
        {
            Errors = new Dictionary<string, string[]>
            {
                { "Error Test", new[] 
                    { "Error Generated In Application Layer"}}
            },
            ErrorType = Error.ValidationProblem
        };
    }
    
    // GET BY ID
    
    public GetExchangeRateByIdQueryResponse GenerateInvalidGetExchangeRatesByIdQueryResponse()
    {
        return new GetExchangeRateByIdQueryResponse
        {
            ExchangeRate = new ExchangeRateDto()
            {
                Id = Guid.NewGuid(),
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(2),
                Type = "Open"
            },
            Errors = new Dictionary<string, string[]>
            {
                { "Error Test", new[] 
                { "Error Generated In Application Layer"}}
            },
            ErrorType = Error.ValidationProblem
        };
    }
    
    public GetExchangeRateByIdQuery GenerateInvalidGetExchangeRatesByIdQuery()
    {
        return new GetExchangeRateByIdQuery(Guid.Empty);
    }
}