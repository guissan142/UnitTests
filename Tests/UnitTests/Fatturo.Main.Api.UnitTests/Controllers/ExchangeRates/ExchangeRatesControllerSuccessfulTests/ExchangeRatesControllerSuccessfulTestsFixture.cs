﻿using Fatturo.Main.Api.Controllers;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Moq.AutoMock;

namespace Fatturo.Main.Api.UnitTests.Controllers.ExchangeRates.ExchangeRatesControllerSuccessfulTests;

public class ExchangeRatesControllerSuccessfulTestsFixture
{
    public AutoMocker Mocker = null!;
    public ExchangeRatesController Controller = null!;
    
    public ExchangeRatesController GenerateAndSetupExchangeRatesController()
    {
        Mocker = new AutoMocker();

        var serviceCollection = new ServiceCollection();
        serviceCollection.AddMvc();
        var serviceProvider = serviceCollection.BuildServiceProvider();
        var httpContext = new DefaultHttpContext { RequestServices = serviceProvider };
        
        Controller = Mocker.CreateInstance<ExchangeRatesController>();
        
        Controller.ControllerContext = new ControllerContext { HttpContext = httpContext };

        var mediatorMock = Mocker.GetMock<IMediator>();

        mediatorMock
            .Setup(m => m.Send(It.IsAny<GetExchangeRateByIdQuery>(), default))
            .ReturnsAsync(GenerateValidGetExchangeRatesByIdQueryResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<CreateExchangeRateCommand>(), default))
            .ReturnsAsync(GenerateValidCreateExchangeRatesCommandResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<DeleteExchangeRatesCommand>(), default))
            .ReturnsAsync(GenerateValidDeleteExchangeRatesCommandResponse());
    
        mediatorMock
            .Setup(m => m.Send(It.IsAny<UpdateExchangeRateCommand>(), default))
            .ReturnsAsync(GenerateValidUpdateExchangeRatesCommandResponse);
        
        return Controller;
    }

    // CREATE
    public CreateExchangeRateCommandResponse GenerateValidCreateExchangeRatesCommandResponse()
    {
        return new CreateExchangeRateCommandResponse
        {
            ExchangeRate = new CreateExchangeRate
            {
                Id = Guid.NewGuid(),
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                Type = "Open"
            }
        };
    }

    public CreateExchangeRateCommand GenerateValidCreateExchangeRatesCommand()
    {
        return new CreateExchangeRateCommand
        {
            CurrencyDestinationId = Guid.NewGuid(),
            CurrencyOriginId = Guid.NewGuid(),
            Factor = 123.123123M,
            QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            Type = "4"
        };
    }
    
    
    // UPDATE
    public UpdateExchangeRateCommand GenerateValidUpdateExchangeRatesCommand()
    {
        return new UpdateExchangeRateCommand(Guid.NewGuid())
        {
            CurrencyDestinationId = Guid.NewGuid(),
            CurrencyOriginId = Guid.NewGuid(),
            Factor = 123.123123M,
            QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            Type = "4"
        };
    }
    
    public UpdateExchangeRatesCommandResponse GenerateValidUpdateExchangeRatesCommandResponse()
    {
        return new UpdateExchangeRatesCommandResponse()
        {
            Errors = new(),
            ErrorType = null
        };
    }
    
    // DELETE
    
    public DeleteExchangeRatesCommand GenerateValidDeleteExchangeRatesCommand()
    {
        return new DeleteExchangeRatesCommand(Guid.NewGuid());
    }
    
    public DeleteExchangeRatesCommandResponse GenerateValidDeleteExchangeRatesCommandResponse()
    {
        return new DeleteExchangeRatesCommandResponse()
        {
            Errors = new(),
            ErrorType = null
        };
    }
    
    // GET BY ID
    
    public GetExchangeRateByIdQueryResponse GenerateValidGetExchangeRatesByIdQueryResponse()
    {
        return new GetExchangeRateByIdQueryResponse
        {
            ExchangeRate = new ExchangeRateDto()
            {
                Id = Guid.NewGuid(),
                CurrencyDestinationId = Guid.NewGuid(),
                CurrencyOriginId = Guid.NewGuid(),
                Factor = 123.123123M,
                QuotationDate = DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                Type = "Open"
            }
        };
    }
    
    public GetExchangeRateByIdQuery GenerateValidGetExchangeRatesByIdQuery()
    {
        return new GetExchangeRateByIdQuery(Guid.NewGuid());
    }

    
}