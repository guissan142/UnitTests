﻿using Fatturo.Main.Api.Controllers;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.CreateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.DeleteExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Commands.UpdateExchangeRates;
using Fatturo.Main.Application.Features.ExchangeRates.Queries.GetExchangeRatesById;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;

namespace Fatturo.Main.Api.UnitTests.Controllers.ExchangeRates.ExchangeRatesControllerSuccessfulTests;

public class ExchangeRatesControllerSuccessfulTests : IClassFixture<ExchangeRatesControllerSuccessfulTestsFixture>
{
    private readonly ExchangeRatesControllerSuccessfulTestsFixture _fixture;
    private readonly ExchangeRatesController _controller;
    private readonly AutoMocker _mocker;

    public ExchangeRatesControllerSuccessfulTests( ExchangeRatesControllerSuccessfulTestsFixture fixture)
    {
        _fixture = fixture;
        _controller = _fixture.GenerateAndSetupExchangeRatesController();
        _mocker = _fixture.Mocker;
    }
    
    [Fact(DisplayName = "Get ExchangeRate By Id : When ExchangeRate Is In Db Test : Should Return Ok ExchangeRate")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "GetExchangeRateById")]
    public async void GetExchangeRateById_WhenExchangeRateIsInDb_ShouldReturnOkExchangeRate()
    {
        var query = _fixture.GenerateValidGetExchangeRatesByIdQuery();

        var response = await _controller.GetExchangeRateById(query.Id);

        response.Should().BeOfType<ActionResult<ExchangeRateDto>>();
        response.Result.Should().BeOfType<OkObjectResult>();
    }
    
    [Fact(DisplayName = "Create ExchangeRate : When Command Is Successful : Should Return CreatedAtRoute ExchangeRate")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "CreateExchangeRate")]
    public async Task CreateExchangeRate_WhenCommandIsSuccessful_ShouldReturnCreatedAtRouteExchangeRate()
    {
        var command = _fixture.GenerateValidCreateExchangeRatesCommand();

        var response = await _controller.CreateExchangeRate(command);


        response.Should().BeOfType<ActionResult<CreateExchangeRate>>();
        response.Result.Should().BeOfType<CreatedAtRouteResult>();
    
    }
    
    [Fact(DisplayName = "Create ExchangeRate : When Command Is Successful : Should Return Correct Values In CreatedAtRoute Result")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "CreateExchangeRate")]
    public async Task CreateExchangeRate_WhenCommandIsSuccessful_ShouldReturnCorrectValuesInCreatedAtRoute()
    {
        var command = _fixture.GenerateValidCreateExchangeRatesCommand();

        var response = await _controller.CreateExchangeRate(command);
    
        var actionResult = Assert.IsType<ActionResult<CreateExchangeRate>>(response);
        var createdAtRouteResult = Assert.IsType<CreatedAtRouteResult>(actionResult.Result);
        var exchangeRateValue = Assert.IsAssignableFrom<CreateExchangeRate>(createdAtRouteResult.Value);
        var dictValue = createdAtRouteResult.RouteValues!.Values.FirstOrDefault();
    
        Assert.Equal(dictValue, exchangeRateValue.Id); // testing is created at route id is equal to value id
    }
    
    [Fact(DisplayName = "Update ExchangeRate : When Params Are Valid And Command Is Successful : Should Return NoContent Result")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "UpdateExchangeRate")]
    public async Task UpdateExchangeRate_WhenParamsAreValidAndCommandIsSuccessful_ShouldReturnNoContent()
    {
        var command = _fixture.GenerateValidUpdateExchangeRatesCommand();

        var response = await _controller.UpdateExchangeRate(command.Id, command);

        response.Should().BeOfType<NoContentResult>();
    }
    
    [Fact(DisplayName = "Update ExchangeRate : When Params Are Valid And Command Is Successful : Should Execute Dependencies Correctly")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "UpdateExchangeRate")]
    public async Task UpdateExchangeRate_WhenParamsAreValidAndCommandIsSuccessful_ShouldExecuteDependenciesCorrectly()
    {
        var command = _fixture.GenerateValidUpdateExchangeRatesCommand();

        var response = await _controller.UpdateExchangeRate(command.Id, command);

        _mocker.GetMock<IMediator>().Verify(m => m.Send(command, default), Times.Once);
    }
    
    [Fact(DisplayName = "Delete ExchangeRate : When Command Is Successful  : Should Return NoContent Result")]
    [Trait("Category", "ExchangeRate Controller Successful Tests")]
    [Trait("Method", "DeleteExchangeRate")]
    public async Task DeleteExchangeRate_WhenMediatorResultIsTrue_ShouldReturnNoContent()
    {
        var command = _fixture.GenerateValidDeleteExchangeRatesCommand();
    
        var response = await _controller.DeleteExchangeRate(command.Id);

        response.Should().BeOfType<NoContentResult>();
    }
    
}