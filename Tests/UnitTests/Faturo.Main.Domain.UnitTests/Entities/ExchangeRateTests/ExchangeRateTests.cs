using Fatturo.Main.Domain.Common;
using Fatturo.Main.Domain.Entities;
using FluentAssertions;
using Xunit.Abstractions;

namespace Faturo.Main.Domain.UnitTests.Entities.ExchangeRateTests;

public class ExchangeRateTests : IClassFixture<ExchangeRateTestsFixture>
{
    private readonly ExchangeRateTestsFixture _fixture;
    private readonly ITestOutputHelper _outputHelper;
        
    public ExchangeRateTests(ExchangeRateTestsFixture fixture, ITestOutputHelper outputHelper)
    {
        _fixture = fixture;
        _outputHelper = outputHelper;
    }
    
    [Fact(DisplayName = "Exchange Rate Constructor Should Round Factor To 5 Decimal Cases")]
    [Trait("Category", "Exchange Rate Tests")]
    public void ExchangeRateConstructor_WhenParametersAreValid_ShouldRoundFactorToFiveDecimalCases()
    {
        // ARRANGE & ACT
        var exchangeRate = new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            23.500009M,
            "Open",
            Guid.NewGuid(),
            Guid.NewGuid());
        
        // ASSERT
        exchangeRate.Factor.Should().Be(23.50001M);
    }
    
    [Fact(DisplayName = "Validate Should Not Throw Exception When Parameters Are Valid")]
    [Trait("Category", "Exchange Rate Tests")]
    public void Validate_WhenParametersAreValid_ShouldNotThrow()
    {
        // ARRANGE & ACT
        Action act = () => _fixture.GenerateValidExchangeRates();
        var exception = Record.Exception(act);

        // ASSERT
        exception.Should().BeNull();
    }
    
    [Fact(DisplayName = "Validate Should Throw Exception When Quotation Date Is In The Future")]
    [Trait("Category", "Exchange Rate Tests")]
    public void Validate_WhenQotationDateIsInTheFuture_ShouldThrow()
    {
        // ARRANGE & ACT
        Action act = () => _fixture.GenerateInvalidQuotationDateExchangeRates();
        var exception = Record.Exception(act);

        // ASSERT
        exception.Should().BeOfType<DomainException>();
        exception.Message.Should().Be("The QuotationDate cannot be in the future");
        _outputHelper.WriteLine($"Exception Message : {exception.Message}");
    }
    
    [Fact(DisplayName = "Validate Should Throw Exception When CurrencyOrigin Id Is Invalid")]
    [Trait("Category", "Exchange Rate Tests")]
    public void Validate_WhenCurrencyOriginIdIsInvalid_ShouldThrowDomainException()
    {
        // ARRANGE & ACT
        Action act = () => _fixture.GenerateInvalidCurrencyOriginIdExchangeRates();
        var exception = Record.Exception(act);

        // ASSERT
        exception.Should().BeOfType<DomainException>();
        exception.Message.Should().Be("The CurrencyOriginId cannot be empty");
        _outputHelper.WriteLine($"Exception Message : {exception.Message}");
    }
    
    [Fact(DisplayName = "Validate Should Throw Exception When Currency Destination Id Is Invalid")]
    [Trait("Category", "Exchange Rate Tests")]
    public void Validate_WhenCurrencyDestinationIdIsInvalid_ShouldThrowDomainException()
    {
        // ARRANGE & ACT
        Action act = () => _fixture.GenerateInvalidCurrencyDestinationIdExchangeRates();
        var exception = Record.Exception(act);

        // ASSERT
        exception.Should().BeOfType<DomainException>();
        exception.Message.Should().Be("The CurrencyDestinationId cannot be empty");
        _outputHelper.WriteLine($"Exception Message : {exception.Message}");
    }
    
    [Fact(DisplayName = "Validate Should Throw Exception When Factor Is Equal To Zero")]
    [Trait("Category", "Exchange Rate Tests")]
    public void Validate_WhenFactorIsZero_ShouldThrowDomainException()
    {
        // ARRANGE & ACT
        Action act = () => _fixture.GenerateInvalidFactorExchangeRates();
        var exception = Record.Exception(act);

        // ASSERT
        exception.Should().BeOfType<DomainException>();
        exception.Message.Should().Be("The Factor cannot be zero");
        _outputHelper.WriteLine($"Exception Message : {exception.Message}");
    }
    
}