using Bogus;
using Fatturo.Main.Domain.Common;
using Fatturo.Main.Domain.Entities;

namespace Faturo.Main.Domain.UnitTests.Entities.ExchangeRateTests;

public class ExchangeRateTestsFixture
{
    
    public ExchangeRate GenerateValidExchangeRates()
    {
        return new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            23.500009M,
            "Ptax",
            Guid.NewGuid(),
            Guid.NewGuid()
        );
    }
    public ExchangeRate GenerateInvalidCurrencyOriginIdExchangeRates()
    {
        return new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            23.500009M,
            "Ptax",
            Guid.Empty,
            Guid.NewGuid()
        );
    }
    public ExchangeRate GenerateInvalidCurrencyDestinationIdExchangeRates()
    {
        return new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            23.500009M,
            "Ptax",
            Guid.NewGuid(),
            Guid.Empty
        );
    }
    public ExchangeRate GenerateInvalidQuotationDateExchangeRates()
    {
        return new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(2),
            23.500009M,
            "Ptax",
            Guid.NewGuid(),
            Guid.NewGuid()
        );
    }
    public ExchangeRate GenerateInvalidFactorExchangeRates()
    {
        return new ExchangeRate(
            DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
            0M,
            "Ptax",
            Guid.NewGuid(),
            Guid.NewGuid()
        );
    }
    
    
}